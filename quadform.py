from math import sqrt
import sys


def quad_formula(a, b, c):
    # Discriminant
    d = b**2 - 4*a*c

    if d > 0:
        pos_x = (-b + sqrt(d))/(2*a)
        neg_x = (-b - sqrt(d))/(2*a)
        print("x =", pos_x)
        print("x =", neg_x)
    elif d == 0:
        x = (-b)/(2*a)
        print("x =", x)
    elif d < 0:
        print("x is not a real number")


try:
    if sys.argv[1] == "-h":
        print("quadform.py <a> <b> <c>")
        sys.exit()
    else:
        try:
            a = float(sys.argv[1])
            b = float(sys.argv[2])
            c = float(sys.argv[3])
        except:
            print("quadform.py -h for help")
            sys.exit()
except SystemExit:
    sys.exit()
except:
    a = float(input("a = "))
    b = float(input("b = "))
    c = float(input("c = "))

    
print()

quad_formula(a, b, c)
